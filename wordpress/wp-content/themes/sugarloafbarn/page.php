<?php
/**
 * Default template for all pages.
 *
 * aka Article template
 *
 * @package WordPress
 * @subpackage The Sugar Loaf Barn Theme
 * @since The Sugar Loaf Barn Theme 1.0
 */

get_header(); ?>

<script>sugarloafbarn.pages.reservationsPage = true;</script>

	<div class="main">
            <div class="breadcrumbs">
                <?php getBreadCrumb(); ?> 
            </div>
        <div class="content">
        <?php
        
            if ( have_posts() ) {
                the_post();
        ?>

            <?php

                echo '<h1 class="heading-text">' . get_the_title(get_the_ID()) . '</h1>';

            ?>
            
            <?php the_content(); ?>
            <div class="clearfix">                                
                <div class="reservations-container clearfix">
                    <select id="reservation-select">
                        <option value="7">Fri 7 days</option>
                        <option value="14">Fri 14 days</option>
                        <option value="3">Fri, Sat, Sun</option>
                        <option value="4">Mon, Tue, Wed, Thu</option>
                        <option value="2">Fri, Sat</option>
                    </select>
                    <div id="calendar"></div>
                    <div class="block-calendar-key">
                        <div class="block-section">
                            <p>
                                <span class="available"></span>
                                Click to book                            
                            </p>
                        </div>
                        <div class="block-section">
                            <p>
                                <span class="unavailable"></span>
                                Unavailable                            
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                   // get data from the booking calendar
                   $data = get_option('wp-simple-booking-calendar-options');                   
                   $specificData = $data[calendars][1][calendarJson];                              
            ?>
            
            <script>
                // set global variable so we can get the calendar data into the js
                data = <?php echo($specificData)?>;                                  
            </script>
                    
        </div>                
        <?php
                }
        ?>

<?php get_footer(); ?>