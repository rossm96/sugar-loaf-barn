<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!DOCTYPE html>
<head>
<meta charset="UTF-8" />

<meta name="viewport" content="width=device-width" />
<title>The Sugar Loaf Barn</title>

<link rel="shortcut icon" href="http://staging.solidstategroup.com/global_business_initiative/wp-content/themes/gbi/images/gbi-favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/header.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/slideshow.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/nav.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/footer.css" />
    
</head>
<body>
    <div class="wrapper">
        <header>
            <div class="header-top clearfix">
                <a class="site-logo" title="Sugar Loaf Barn - Relax and unwind">Sugar Loaf Barn - Relax and unwind</a>
                <div class="social-media-icons">
                    <ul>
                        <li class="facebook"><a href="">Facebook</a></li>
                        <li class="twitter"><a href="">Twitter</a></li>
                    </ul>
                </div>            
                <p class="contact-info">Bookings And Reservations:  +44 01552 832 789</p>
            </div>
            <nav class="mainNav clearfix">
                <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="">THINGS TO DO</a></li>
                    <li><a href="accomodation.php">ACCOMODATION</a></li>
                    <li><a href="">LOCATION</a></li>
                    <li><a href="aggregation.php">NEWS</a></li>
                    <li><a href="gallery.php">GALLERY</a></li>
                    <li><a href="">RESERVATIONS</a></li>
                    <li><a href="contact.php">CONTACT</a></li>					
                </ul>
            </nav>
        </header>
        <div class="main">
            <div class="breadcrumbs">
                <p><a href="">Home</a> 	&gt; Gallery</p>
            </div>
            <div class="content">
                <h1 class="heading-text">GALLERY</h1>
                <div class="gallery clearfix">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                    <img src="images/dining-area.jpg">
                </div>            
            </div>
            
            <footer class="page-footer">
                <p>COPYRIGHT 2011 THE SUGAR LOAF BARN SITE DESIGN AND DEVELOPMENT ROSS MATTHEWS 2012 POWERED BY WORDPRESS</p>
            </footer>
        </div>
    </div>
</body>
</html>

