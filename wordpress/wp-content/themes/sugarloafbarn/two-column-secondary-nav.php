<?php
/**
 * Template Name: Two Column Secondary Navigation
 *
 * 
 *
 * @package WordPress
 * @subpackage The Sugar Loaf Barn Theme
 * @since The Sugar Loaf Barn Theme 1.0
 */

get_header(); ?>

	<div class="main clearfix">
            <div class="leftCol clearfix">
                <nav class="secNav">
                <?php
                    if($post->post_parent) {
                        $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
                    } else {
                        $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
                    } if ($children) { ?>
                        <ul>
                            <?php echo $children; ?>
                        </ul>
                <?php } ?>                
                </nav>            
            </div>
            <div class="mainCol">
                <div class="breadcrumbs">
                    <?php getBreadCrumb(); ?> 
                </div>               
                <div class="content">
                    <?php
                        if ( have_posts() ) {
                            the_post();

                            echo '<h1 class="heading-text">' . get_the_title(get_the_ID()) . '</h1>';
                    ?>

                    <?php the_content(); ?>
                </div>                               
            </div>
        <?php
                }
        ?>

<?php get_footer(); ?>