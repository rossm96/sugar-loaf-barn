sugarloafbarn.register('pages.reservations', {
    init : function() {                
        this.setupAvailabilityDates();
        this.setupCalendar();
        this.setupLengthOfStayChange();
    },
    
    // setup the available date array
    setupAvailabilityDates : function(){
        
        var years = [],
            currentYear = {},
            currentMonth = {},
            yearStr = '',
            monthStr = '',
            dayStr = '',
            dates = [];

        // get the years     
        for(key in data) {                    
            years.push(key);
        }

        for (var i=0;i<years.length;i++) {
            currentYear = data[years[i]];

            for (month in currentYear){
                currentMonth = currentYear[month];

                for (day in currentMonth) {
                    yearStr = years[i].replace('year', '');
                    monthStr = month.replace('month', '');
                    dayStr = day.replace('day', '');

                    dates.push(moment(dayStr + ' ' + monthStr + ' ' + yearStr, "D MM YYYY")); 
                }
            }
        }
        //use this for testing console.log('Here are the unavailable dates');
        //for (var k = dates.length-1; k > -1; k--){
            //console.log(dates[k]);
        //}       
        return dates;
    },    
    
    
    
    setupLengthOfStayChange : function(){
        $('#reservation-select').change(function() {
            // select has changed, re-render the calendar
            $('#calendar').fullCalendar('render');
        });
    },
    
    getDayFlags : function(dayFlags){
        
        var unavailableDates = this.setupAvailabilityDates();
        var unavailableDatesLength = unavailableDates.length;
        var days = [];
        var daysToCheck = [];
                      
        // set our array of days for the next year
        var now = new Date();
        now.setHours(0,0,0,0);
                
        for (var i=0; i<=365; i++){
            days[i] = moment(now).add('days', i);
            days[i].value = days[i].valueOf();
        }
                              
        for (var i=0; i<365; i++) {           
            for (var j=0; j<unavailableDatesLength; j++) {
                
                // set unavailable flag
                if (days[i].value == unavailableDates[j].valueOf()) {
                    days[i].unavailable = true;
                }            
            }
        }
         
         // set dates data
         for (var i=0; i<365; i++) {                        
            if (!days[i].unavailable) {                             
                
                    // if the day is friday    
                    if (days[i].day() == 5) {
                                          
                        // check if any of the next 7 days are unavailable                  
                        for (var j=1; j<7; j++){
                            var date = 0;
                            var date = i + j;
                            
                            if (days[date]){
                                if (days[date].unavailable){
                                    days[i].sevenDayAvailable = false;
                                    break;
                                } else {
                                    days[i].sevenDayAvailable = true;
                                    //console.log(days[i]);
                                }
                            }
                        }
                        
                        // check if any of the next 14 days are unavailable                  
                        for (var j=1; j<14; j++){
                            var date = 0;
                            var date = i + j;
                            
                            if (days[date]){
                                if (days[date].unavailable){
                                    days[i].fourteenDayAvailable = false;
                                    break;
                                } else {
                                    days[i].fourteenDayAvailable = true;
                                    //console.log(days[i]);
                                }
                            }
                        }
                        
                        // check if any of the next 3 days are unavailable                  
                        for (var j=1; j<3; j++){
                            var date = 0;
                            var date = i + j;
                            
                            if (days[date]){
                                if (days[date].unavailable){
                                    days[i].threeDayAvailable = false;
                                    break;
                                } else {
                                    days[i].threeDayAvailable = true;
                                    //console.log(days[i]);
                                }
                            }
                        }
                        
                        // check if any of the next 2 days are unavailable                  
                        for (var j=1; j<2; j++){
                            var date = 0;
                            var date = i + j;                           
                            
                            if (days[date]){
                                if (days[date].unavailable){
                                    days[i].twoDayAvailable = false;
                                    break;
                                } else {
                                    days[i].twoDayAvailable = true;
                                }
                            }
                        }                        
                    }                                              
   
                    if (days[i].day() == 1) {                                          
                        // check if any of the next 4 days are unavailable                  
                        for (var j=1; j<4; j++){
                            var date = 0;
                            var date = i + j;
                            
                            if (days[date]){
                                if (days[date].unavailable){
                                    days[i].fourDayAvailable = false;
                                    break;
                                } else {
                                    days[i].fourDayAvailable = true;
                                }
                            }
                        }
                    }
                    // for logging day
                    // console.log(days[i]);                
            }
            
            // for debugging date availability
            // if (days[i].isSame('2013-11-29')) {
            //    console.log(days[i]);
            // }
        }           
        return days;
    },   
    
    setupCalendar : function(){       
        var that = this;
        
        if (data) {
            var unavailableDates = this.setupAvailabilityDates(),
                unavailableDatesLength = unavailableDates.length,
                calendar = $('#calender'),
                days = [],
                lengthOfStay = '',
                dayMoments = this.getDayFlags();
                dayFlagValues = [];
                dayFlagArrayIndex = 0;
                
                //console.log(dayMoments);
                for (var i = 0; i<dayMoments.length; i++){
                    dayFlagValues[i] = dayMoments[i].value;
                }                               
                                      
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev',
                    center: 'title',
                    right: 'next'
                },
                firstDay: 5,

                // set calendar to only display the next 12 months
                viewDisplay : function(view) {
                    
                    console.log('calendar rendered!');

                    var now = new Date(),
                        end = new Date(),
                        then = moment(now).add('days', 365);

                    now.setHours(0,0,0,0);
                    end.setHours(0,0,0,0);
                    end.setMonth(now.getMonth() + 12);

                    var cal_date_string = view.start.getMonth()+'/'+view.start.getFullYear();
                    var cur_date_string = now.getMonth()+'/'+now.getFullYear();
                    var end_date_string = end.getMonth()+'/'+end.getFullYear();

                    if (cal_date_string == cur_date_string) {
                        jQuery('.fc-button-prev').addClass("fc-state-disabled");
                        jQuery('.fc-button-prev').parent().addClass("disabled");
                    }
                    else {
                        jQuery('.fc-button-prev').removeClass("fc-state-disabled");
                        jQuery('.fc-button-prev').parent().removeClass("disabled");
                    }

                    if (end_date_string == cal_date_string) {
                        jQuery('.fc-button-next').addClass("fc-state-disabled");
                        jQuery('.fc-button-next').parent().addClass("disabled");
                    }
                    else {
                        jQuery('.fc-button-next').removeClass("fc-state-disabled");
                        jQuery('.fc-button-next').parent().removeClass("disabled");
                    }

                    var tableDayCells = $(this).find('td.fc-day');

                    tableDayCells.each(function (index, el) {
                        var $el = $(el),
                            dateIsValid = true,
                            unavailableDate = false,
                            dateString = $el.attr('data-date');                           

                        // find the divs with an inline min-height style and set to auto - as above
                        //if ($el.find('div').attr('style')) {
                            $el.find('div').css('min-height', 'inherit');
                            //$el.find('div').css();
                           
                        //}
                        
                        dateStringMoment = new moment(dateString, "YYYY-MM-DD");
                        dateString = new moment(dateString, "YYYY-MM-DD").valueOf();
                        
                        dayFlagArrayIndex = jQuery.inArray(dateString, dayFlagValues);
                        
                        //if (dayFlagArrayIndex != -1) {
                            //console.log(dayMoments[dayFlagArrayIndex]);
                        //}
                        
                        if (moment(dateString).isBefore(now) || moment(dateString).isAfter(then)) {
                            $(this).addClass('mute');
                            dateIsValid = false;
                            
                        } else {
                            $(this).removeClass('book-option');
                            //console.log(dayMoments[dayFlagArrayIndex])
                            if (dayMoments[dayFlagArrayIndex].unavailable) {
                                $(this).addClass('not-available');
                                unavailableDate = true;    
                            
                            } else {
                                 
                                if ($('#reservation-select').val() == 7){
                                    if (dayMoments[dayFlagArrayIndex].sevenDayAvailable) {
                                        $(this).addClass('book-option');
                                    }
                                    
                                } else if($('#reservation-select').val() == 14) {
                                    if (dayMoments[dayFlagArrayIndex].fourteenDayAvailable) {
                                        $(this).addClass('book-option');
                                    }                                
                                
                                } else if($('#reservation-select').val() == 3) {
                                    if (dayMoments[dayFlagArrayIndex].threeDayAvailable) {
                                        $(this).addClass('book-option');
                                    }
                                    
                                } else if($('#reservation-select').val() == 4) {
                                    if (dayMoments[dayFlagArrayIndex].fourDayAvailable) {
                                        $(this).addClass('book-option');
                                    }
                                    
                                } else if($('#reservation-select').val() == 2) {                                    
                                    if (dayMoments[dayFlagArrayIndex].twoDayAvailable) {
                                        $(this).addClass('book-option');
                                    }                                    
                                }                                                              
                            }
                                                       
//                            for (var i = 0; i < unavailableDatesLength; i++) {
//                                
//                                if (dateString == unavailableDates[i].valueOf()) {
//                                    
//                                    $(this).addClass('not-available');
//                                    unavailableDate = true;
//                                    break;
//                                }                               
//                            }
                        }
                    });                    
                },
                
                dayClick : function(date, allDay, jsEvent, view) { 
                    
                    var dateOfStayInput = $('input[name="date-of-stay"]');
                    var lengthOfStayInput = $('input[name="length-of-stay"]');
                    var startDate;
                    var endDate;
                    
                    console.log(dateOfStayInput);
                    console.log(lengthOfStayInput);
                    
                    if (!$(this).hasClass('book-option')) {
                        return;
                        
                    } else if ($('#reservation-select').val()){
                        // fri 7 days
                        startDate = moment(date);
                        endDate = moment(date).add('days', $('#reservation-select').val());
                        
                        lengthOfStayInput.val($('#reservation-select').val() + ' days');
                        dateOfStayInput.val(startDate.format('dddd, MMMM Do YYYY') + ' - ' + endDate.format('dddd, MMMM Do YYYY')); 
                    }                      
                }              
            });        
        }
    }
});

