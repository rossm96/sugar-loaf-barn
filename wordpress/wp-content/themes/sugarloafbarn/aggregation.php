<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!DOCTYPE html>
<head>
<meta charset="UTF-8" />

<meta name="viewport" content="width=device-width" />
<title>The Sugar Loaf Barn</title>

<link rel="shortcut icon" href="http://staging.solidstategroup.com/global_business_initiative/wp-content/themes/gbi/images/gbi-favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/header.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/slideshow.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/nav.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/footer.css" />
    
</head>
<body>
    <div class="wrapper">
        <header>
            <div class="header-top clearfix">
                <a class="site-logo" title="Sugar Loaf Barn - Relax and unwind">Sugar Loaf Barn - Relax and unwind</a>
                <div class="social-media-icons">
                    <ul>
                        <li class="facebook"><a href="">Facebook</a></li>
                        <li class="twitter"><a href="">Twitter</a></li>
                    </ul>
                </div>            
                <p class="contact-info">Bookings And Reservations:  +44 01552 832 789</p>
            </div>
            <nav class="clearfix mainNav">
                <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="">THINGS TO DO</a></li>
                    <li><a href="accomodation.php">ACCOMODATION</a></li>
                    <li><a href="">LOCATION</a></li>
                    <li><a href="aggregation.php">NEWS</a></li>
                    <li><a href="gallery.php">GALLERY</a></li>
                    <li><a href="">RESERVATIONS</a></li>
                    <li><a href="contact.php">CONTACT</a></li>					
                </ul>
            </nav>
        </header>
        <div class="main">
            <div class="breadcrumbs">
                <p><a href="">Home</a> 	&gt; News</p>
            </div>
            <div class="content bordered">
                <h1 class="heading-text">NEWS</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </div>
            <div class="fullAggregation aggregationResults">
                <article class="aggregationItem clearfix">
                    <img class="aggregationThumb" src="images/rider-thumb-large.jpg">
                    <h2>Title here</h2>
                    <span class="time">16th March 2012</>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ullamco laboris nisi ut aliquip ex modo. consequat diri cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official dolor sit amer. Amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua........</p>    
                </article>
                <article class="aggregationItem clearfix">
                    <img class="aggregationThumb" src="images/rider-thumb-large.jpg">
                    <h2>Title here</h2>
                    <span class="time">16th March 2012</>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ullamco laboris nisi ut aliquip ex modo. consequat diri cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official dolor sit amer. Amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua........</p>    
                </article>
                <article class="aggregationItem clearfix">
                    <img class="aggregationThumb" src="images/rider-thumb-large.jpg">
                    <h2>Title here</h2>
                    <span class="time">16th March 2012</>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ullamco laboris nisi ut aliquip ex modo. consequat diri cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official dolor sit amer. Amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua........</p>    
                </article> 
                <article class="aggregationItem clearfix">
                    <img class="aggregationThumb" src="images/rider-thumb-large.jpg">
                    <h2>Title here</h2>
                    <span class="time">16th March 2012</>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ullamco laboris nisi ut aliquip ex modo. consequat diri cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official dolor sit amer. Amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua........</p>    
                </article> 
                <article class="aggregationItem clearfix">
                    <img class="aggregationThumb" src="images/rider-thumb-large.jpg">
                    <h2>Title here</h2>
                    <span class="time">16th March 2012</>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ullamco laboris nisi ut aliquip ex modo. consequat diri cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official dolor sit amer. Amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua........</p>    
                </article> 
                <article class="aggregationItem clearfix">
                    <img class="aggregationThumb" src="images/rider-thumb-large.jpg">
                    <h2>Title here</h2>
                    <span class="time">16th March 2012</>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ullamco laboris nisi ut aliquip ex modo. consequat diri cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official dolor sit amer. Amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua........</p>    
                </article>                 
            </div>
            <footer class="page-footer">
                <p>COPYRIGHT 2011 THE SUGAR LOAF BARN SITE DESIGN AND DEVELOPMENT ROSS MATTHEWS 2012 POWERED BY WORDPRESS</p>
            </footer>
        </div>
    </div>
</body>
</html>


