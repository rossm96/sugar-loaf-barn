<?php

function get_childTheme_url() {
    return dirname( get_bloginfo('stylesheet_url') );
}

add_action( 'init', 'register_my_menus' );
function register_my_menus() {
    register_nav_menus( array(
        'primary' => __( 'Primary Navigation' )
    )
  );
}


/* enable post thumbnails */
add_theme_support( 'post-thumbnails' );

// Generate a new thumbnail with our desired name and size
if (function_exists ( 'add_image_size' ) ) {
    add_image_size( 'smallest', 95, 46 );
}    


add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

// makes a custom limit words function for strings
function string_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}


/*
 * =============================================
 * 
 *	WIDGET AREAS
 *
 * =============================================
 */

if ( function_exists('register_sidebar') )
    register_sidebar(array(
    'name' => 'Footer'
));

if(function_exists('register_sidebar'))
      register_sidebar(array(
      'name' => 'Homepage Sidebar',
));

/**
 * Custom Footer Text widget class
 *
 * @since 2.8.0
 */
class Footer_Text_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
	 		'footer_text_widget', // Base ID
			'Footer Text Widget', // Name
			array( 'description' => __( 'Footer text widget', 'Footer text elements' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		//$title = apply_filters( 'widget_title', $instance['title'] );
        $text_line_one = $instance['text_line_one'];
        $text_line_two = $instance['text_line_two'];

		if ( ! empty( $text_line_one ) )
			echo '<p>' .$text_line_one. '</p>';

        if ( ! empty( $text_line_two ) )
            echo '<p>' .$text_line_two.'<p>';

	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['text_line_one'] = strip_tags( $new_instance['text_line_one'] );
        $instance['text_line_two'] = strip_tags( $new_instance['text_line_two'] );

		return $instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'text_line_one' ] ) ) {
			$text_line_one = $instance[ 'text_line_one' ];
		}
		else {
			$text_line_one = __( 'Insert text line one here', 'text_domain' );
		}
		if ( isset( $instance[ 'text_line_two' ] ) ) {
			$text_line_two = $instance[ 'text_line_two' ];
		}
		else {
			$text_line_two = __( 'Insert text line two here', 'text_domain' );
		}

		?>
		    <p>
		        <label for="<?php echo $this->get_field_id( 'text_line_one' ); ?>"><?php _e( 'Text Line One:' ); ?></label>
		        <input class="widefat" id="<?php echo $this->get_field_id( 'text_line_one' ); ?>" name="<?php echo $this->get_field_name( 'text_line_one' ); ?>" type="text" value="<?php echo esc_attr( $text_line_one ); ?>" />
		    </p>
		    <p>
		        <label for="<?php echo $this->get_field_id( 'text_line_two' ); ?>"><?php _e( 'Text Line Two:' ); ?></label>
		        <input class="widefat" id="<?php echo $this->get_field_id( 'text_line_two' ); ?>" name="<?php echo $this->get_field_name( 'text_line_two' ); ?>" type="text" value="<?php echo esc_attr( $text_line_two ); ?>" />
		    </p>
		<?php
	}

}

add_action('widgets_init', create_function('', "register_widget('Footer_Text_Widget');"));

/* End of widget code */



/* custom function to grab the excerpt outside of the loop */
function get_my_excerpt($id=false) {
    global $post;

    $old_post = $post;
    if ($id != $post->ID) {
        $post = get_page($id);
    }

    if (!$excerpt = trim($post->post_excerpt)) {
        $excerpt = $post->post_content;
        $excerpt = strip_shortcodes( $excerpt );
        $excerpt = apply_filters('the_content', $excerpt);
        $excerpt = str_replace(']]>', ']]&gt;', $excerpt);
        $excerpt = strip_tags($excerpt);
        $excerpt_length = apply_filters('excerpt_length', 55);
        $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');

        $words = preg_split("/[\n\r\t ]+/", $excerpt, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
        if ( count($words) > $excerpt_length ) {
            array_pop($words);
            $excerpt = implode(' ', $words);
            $excerpt = $excerpt . $excerpt_more;
        } else {
            $excerpt = implode(' ', $words);
        }
    }

    $post = $old_post;

    return $excerpt;
}

function getBreadCrumb() {
    if(function_exists('bcn_display')) {
        bcn_display();
    }
}

/*
 * =============================================
 * 
 *	WYSIWYG EDITOR
 *
 * =============================================
 */

function my_editor_style($url) {

  if ( !empty($url) )
    $url .= ',';

  // Change the path here if using sub-directory
  $url .= trailingslashit( get_stylesheet_directory_uri() ) . 'css/editor-style.css';

  return $url;
}

add_filter('mce_css', 'my_editor_style');

/* 
    ADD EXTRA STYLES TO WYSIWYG 
*/

// custom editor styles -- create editor-style.css in your theme directory
add_editor_style();

function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

// add style selector drop down
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

function my_mce_before_init( $settings ) {

    $settings['theme_advanced_blockformats'] = 'p,h1,h2,h3,h4,h5,h6';
    $settings['theme_advanced_buttons1'] = 'bold,italic,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,wp_more,|,spellchecker,fullscreen,wp_adv';
    $settings['theme_advanced_buttons2'] = 'formatselect,styleselect,justifyfull,|,pastetext,pasteword,removeformat,|,media,charmap,|,outdent,indent,|,undo,redo,wp_help';

    $style_formats = array(
        array(
            'title' => 'Call to action button',
            'block' => 'a',
            'classes' => 'silver ctaButton'
        ),
        array(
            'title' => 'Red text',
            'inline' => 'span',
            'classes' => 'redText'
        ),
        array(
            'title' => 'Grey text',
            'inline' => 'span',
            'classes' => 'greyText'
        )
            
    );
    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

}

add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );

/*
 * =============================================
 * 
 *	JS INCLUDES
 *
 * =============================================
 */
function my_js_scripts() {
 
    wp_deregister_script( 'jquery' ); 

    // Register from Google's CDN  
//    wp_register_script( 'jquery', get_template_directory_uri() . '/js/3rdparty/jquery.js', array(), null, true);
//    wp_enqueue_script('jquery');
    
    
    // my scripts             
    
//    wp_register_script('loop', get_template_directory_uri() . '/js/3rdparty/Loop.js', array('mootools'), null, true);    
//    wp_enqueue_script('loop');

//    wp_register_script('slideshow', get_template_directory_uri() . '/js/3rdparty/SlideShow.js', array('mootools', 'loop'), null, true);    
//    wp_enqueue_script('slideshow');
    
    // Only load slimbox on gallery page, as clashes with the booking plugin right now
//    if ( is_page ( 'Gallery' ) ) {
//        wp_register_script('slimbox', get_template_directory_uri() . '/js/3rdparty/slimbox.js', array('mootools'), null, true);    
//        wp_enqueue_script('slimbox');
//    }
     wp_register_script('jquery-ui', 'http://code.jquery.com/ui/1.10.3/jquery-ui.js', null, true);
     wp_enqueue_script('jquery-ui');
    
     wp_register_script('moment', get_template_directory_uri() . '/js/3rdparty/moment.js', null, true);
     wp_enqueue_script('moment');    
    
     wp_register_script( 'slideshow', get_template_directory_uri() . '/js/3rdparty/bjqs-1.3.js', null, true);
     wp_enqueue_script('slideshow');
     
     wp_register_script('gallery', get_template_directory_uri() . '/js/3rdparty/magnific.js', null, true);
     wp_enqueue_script('gallery');
     
     wp_register_script('calendar', get_template_directory_uri() . '/js/3rdparty/fullcalendar.min.js', null, true);    
     wp_enqueue_script('calendar');       
     
     wp_register_script('reservations', get_template_directory_uri() . '/js/pages/reservations.js', null, true);    
     wp_enqueue_script('reservations');     
    
     wp_register_script('ui', get_template_directory_uri() . '/js/ui.js', null, true);
     wp_enqueue_script('ui');         
        
//    wp_register_script('sugarloafbarn', get_template_directory_uri() . '/js/sugar-loaf-barn/sugarloafbarn.js', array('jquery' ), null, true);
//    wp_enqueue_script('sugarloafbarn');    
    
//    wp_register_script('init', get_template_directory_uri() . '/js/init.js', array('jquery', 'sugarloafbarn'), null, true);
//    wp_enqueue_script('init');

}    
 
add_action('wp_enqueue_scripts', 'my_js_scripts');

?>
