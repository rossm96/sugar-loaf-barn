<?php
    //include_once('GBI.php');
?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" dir="ltr" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" dir="ltr" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" dir="ltr" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html dir="ltr" lang="en-US">
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>

<link rel="shortcut icon" href="<?php bloginfo('template_directory')?>/images/favicon.ico" type="image/x-icon"/>
<link rel="profile" href="http://gmpg.org/xfn/11" />

<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/reset.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/sidebar.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/header.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/nav.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/footer.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/form.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/slimbox.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/bjqs.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/slideshow.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/magnific.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/calendar.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/css/calendar-custom.css" />

<script src="<?php bloginfo('template_directory')?>/js/3rdparty/jquery.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory')?>/js/sugar-loaf-barn/sugarloafbarn.js" type="text/javascript"></script>

<!--[if IE]>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory')?>/css/ie.css" />
<![endif]-->

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!--[if lt IE 9]>
	<script src="<?php bloginfo('template_directory')?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php if ( is_page ( 'Reservations' ) ) { ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>

<?php } ?>
    
<?php wp_head(); ?>
    
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17993030-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<meta name="google-site-verification" content="yhy33k6QRHmd6Wa5l2pFMjtgVcE4cUk6IYtQHeGXpRg" />
</head>
<body>
    <div class="wrapper">
        <header>
            <div class="header-top clearfix">                                    
                <a class="site-logo" href="<?php echo bloginfo('siteurl')?>" title="Sugar Loaf Barn - Relax and unwind">Sugar Loaf Barn - Relax and unwind</a>
                <a class="sponsor-logo" target="_blank" href="http://www.visitwales.co.uk"><img src="<?php bloginfo('template_directory')?>/images/wales-tourist-board-small.jpg" alt="Wales Tourist Board Logo" title="http://www.visitwales.co.uk" height="38" width="108"></a>                
                <div class="social-media-icons">
                     <ul>
                        <li class="facebook"><a target="_blank" href="http://www.facebook.com/pages/Sugar-Loaf-Barn/388529351221349?fref=ts">Facebook</a></li>
                        <li class="pinterest"><a target="_blank" href="http://pinterest.com/marcusjohnstone/">Pinterest</a></li>
                        <li class="twitter"><a target="_blank" href="http://www.twitter.com/sugarloaf_barn/">Twitter</a></li>
                    </ul>                   
                </div>
                <p class="contact-info"><?php bloginfo( 'description' ); ?></p>
            </div>
            <nav class="mainNav clearfix">
               <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
            </nav>            
        </header><!-- #branding -->
        <div class="main">
            