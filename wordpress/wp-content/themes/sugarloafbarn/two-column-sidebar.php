<?php
/**
 * Template Name: Two Column Sidebar
 *
 * i.e the Location page
 *
 * @package WordPress
 * @subpackage The Sugar Loaf Barn Theme
 * @since The Sugar Loaf Barn Theme 1.0
 */

get_header(); ?>

	<div class="main clearfix">
            <div class="mainCol left-aligned sidebar">
                <div class="breadcrumbs">
                    <?php getBreadCrumb(); ?> 
                </div>
                <div class="content bordered">
                <?php

                    if ( have_posts() ) {
                        the_post();
                ?>

                <?php

                    echo '<h1 class="heading-text">' . get_the_title(get_the_ID()) . '</h1>';

                ?>

                <?php the_content(); ?>
            
                </div>                
            </div>
            <div class="sidebar-col">
                <div class="widget news-agg">
                    <h4>News</h4>
                <?php
                    // The Query
                    $the_query = new WP_Query( array ('posts_per_page' => 3, 'orderby' => 'date' ) );

                    // The Loop
                    while ( $the_query->have_posts() ) : $the_query->the_post();                   
                ?>
                    <div class="news-item">
                        <a href="<?php echo get_permalink(); ?>">
                            <h5><?php echo the_title();?></h5>                   
                        </a>
                        
               <?php
                       if ( has_post_thumbnail()) {
               ?>            
                            <a href="<?php echo get_permalink(); ?>">
                                <?php echo get_the_post_thumbnail($post->ID, array(58, 28)) ?>
                            </a>
               <?php  
                        }
               ?>
                 
                        <a href="<?php echo get_permalink(); ?>">                       
                            <span class="time"><?php echo get_the_time('jS F Y'); ?></span>
                        </a>
                <?php             
                        $excerpt = get_the_excerpt();               
                        if (!empty($excerpt)) {            

                            $truncatedExcerpt = string_limit_words($excerpt, 12);                        
                ?>
                        
                        <a href="<?php get_permalink() ?>">
                            <p><?php echo $truncatedExcerpt; ?> </p>
                        </a>      
                <?php
                        }
                ?>
                    </div>
                <?php
                    endwhile;
                    
                    // Reset Post Data
                    wp_reset_postdata();                    
                ?>
                </div>
            </div>
        </div>
        <?php } ?>

<?php get_footer(); ?>