<?php
/**
 * Template Name: Gallery
 *
 * 
 *
 * @package WordPress
 * @subpackage The Sugar Loaf Barn Theme
 * @since The Sugar Loaf Barn Theme 1.0
 */

get_header(); ?>
    <div class="breadcrumbs">
        <?php getBreadcrumb();?>
    </div>

<?php 

    if ( have_posts() ) {   
        the_post();
?>

    <div class="content">
        <h1 class="heading-text"><?php the_title()?></h1>
        <?php echo the_content(); ?>

<?php

        $pageId = $post->ID;
        $gallery = simple_fields_get_post_group_values($pageId , 3, true, 2);
        $imgFull = '';
        
        if (!empty($gallery)) {
            
            $galleryOutput = '';
            $galleryOutput .= '<div class="gallery clearfix">';
        
            foreach ($gallery as $galleryImage) {                                                
               
                if (!empty($galleryImage['Gallery Image Full'])) {
                    
                    $imgFull = wp_get_attachment_image_src($galleryImage['Gallery Image Full'], 'full');                    
                    $galleryOutput .= '<a href="' . $imgFull[0] . '" rel="lightbox-gallery"';

                    if (!empty($galleryImage['Gallery Image Description'])) {   
                        $galleryOutput .= 'title="' . $galleryImage['Gallery Image Description']. '" ';                
                    }

                    $galleryOutput .= '>';

                    if (!empty($galleryImage['Gallery Image Thumbnail'])) {

                        $imgThumb = wp_get_attachment_image_src($galleryImage['Gallery Image Thumbnail'], 'full');
                        $galleryOutput .= '<img src="' . $imgThumb[0] . '" >';                   
                    }

                    $galleryOutput .= '</a>';

                }               
           
            }
            $galleryOutput .= '</div>';
            echo $galleryOutput;
        } 
    }
?>

        </div>            
    </div>
<?php get_footer(); ?>