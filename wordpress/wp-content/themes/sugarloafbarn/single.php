<?php
/**
 * Default template for all posts
 *
 * aka Article template
 *
 * @package WordPress
 * @subpackage The Sugar Loaf Barn Theme
 * @since The Sugar Loaf Barn Theme 1.0
 */

get_header(); ?>

	<div class="main">
            <div class="breadcrumbs">
                <?php getBreadCrumb(); ?> 
            </div>
        <div class="content">
        <?php
        
            if ( have_posts() ) {
                the_post();
        ?>

            <?php

                echo '<h1 class="heading-text">' . get_the_title(get_the_ID()) . '</h1>';

            ?>
            
            <?php the_content(); ?>
            
        </div>                
        <?php
                }
        ?>

<?php get_footer(); ?>