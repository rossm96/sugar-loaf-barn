=== Flamingo ===
Contributors: takayukister, megumithemes
Tags: bird, contact, mail, crm
Requires at least: 3.3
Tested up to: 3.4.1
Stable tag: 1.0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Flamingo manages your contact list on WordPress.

== Description ==

Flamingo is a WordPress plugin created to be a total CRM package. With this version, you can manage your contact list and messages submitted via contact form plugins. It has not yet matured, but we are enhancing it rapidly.

= Translators =

* Brazilian Portuguese (pt_BR) - [Ilton Alberto Junior](https://twitter.com/iltonalberto)
* Dutch (nl_NL) - [TenSheep](http://tensheep.nl/)
* Japanese (ja) - [Takayuki Miyoshi](http://ideasilo.wordpress.com)

If you have created your own language pack, or have an update of an existing one, you can send [gettext PO and MO files](http://codex.wordpress.org/Translating_WordPress) to [me](http://ideasilo.wordpress.com/about/) so that I can bundle it into Flamingo. You can download the latest [POT file](http://plugins.svn.wordpress.org/flamingo/trunk/languages/flamingo.pot).

= Icon Design =

* [Takao Honda](http://www.takaohonda.jp/)

== Installation ==

1. Upload the entire `flamingo` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 1.0.1 =

* Fixed: Updating irrelevant posts when adding inbound messages.
* Fixed: Issues relating to post boxes and admin screen options.
* Fixed: Generating unnecessary rewrite rules.
* Translations for Dutch and Brazilian Portuguese have been created.

== Upgrade Notice ==
